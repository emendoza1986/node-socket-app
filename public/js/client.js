var socket = io();

//sound support
var chatSounds = document.getElementById("chatSounds");

function scrollToBottom () {
    // Selectors
    var messages = jQuery('#messages');
    var newMessage = messages.children('li:last-child');
    // Heights
    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight();

    if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
        messages.scrollTop(scrollHeight);
    }
}




socket.on('connect', function () {
    console.log('Connected to server');

    var params = $.deparam(window.location.search);
    socket.emit('join',params,function(err){
        if(err){
            alert(err)
            window.location.href = "/";
        }else{
            console.log('No Join error')
        }
    })


});

socket.on('disconnect', function () {
    console.log('Disconnected from server');
});


socket.on('updateUserList', function(users){
    console.log('Users list', users)
    var ol = $('<ol></ol>');

    users.forEach(function(user){
        ol.append($('<li></li>').text(user))
    });

    $('#users').html(ol);
});

socket.on('newEmail', function (response) {
    console.log('email arrived:', response)
});

socket.on('newMessage', function (message) {
    chatSounds.play();
    var formattedTime = moment(message.createdAt).format('h:mm a');
    var template = $('#message-template').html();
    var html = Mustache.render(template,{
        from: message.from,
        text: message.text,
        createdAt: formattedTime
    });
    $('#messages').append(html);
    scrollToBottom();
});


// socket.emit('createMessage',{
//     from:'frank',
//     text:'hi'
// },function (data){
//     console.log('got it', data);
// });

socket.on('newLocationMessage',function(message){
    var formattedTime = moment(message.createdAt).format('h:mm a');
    var template = $('#location-message-template').html();
    var html = Mustache.render(template,{
        from: message.from,
        url: message.url,
        createdAt: formattedTime
    });
    $('#messages').append(html);
    scrollToBottom();
});


$('#message-form').on('submit', function (e){
   e.preventDefault();
   var messageTextbox = $('[name=message]');
   socket.emit('createMessage',{
       from:'client',
       text:messageTextbox.val()
   }, function (){
       messageTextbox.val('')//set to blank
   })
});

var locationButton = $('#send-location');
locationButton.on('click',function(){
   if(!navigator.geolocation){
       return alert('Geolocation not supported by your browser.')
   }
   locationButton.attr('disabled','disabled').text('Sending location...');

   navigator.geolocation.getCurrentPosition(function(position){
       locationButton.removeAttr('disabled').text('Send Location');
        socket.emit('createLocationMessage',{
            lat: position.coords.latitude,
            lng: position.coords.longitude
        });
   },function(err){
       locationButton.removeAttr('disabled').text('Send Location');
       alert('Unable to fetch location.')
   })
});

