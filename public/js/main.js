var socket = io();

var connected = false;

//leaflet map var
var markers = new Map();
var mymap = L.map('mapid').setView([35.231360, -119.259030], 19);
// L.tileLayer('map/all/{x}-{y}-{z}.png',{    maxZoom: 19  }).addTo(mymap);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png',{    maxZoom: 19  }).addTo(mymap);
var imageUrl = 'map/LIB-2019-main.jpg',
    imageBounds = [[35.233899, -119.269579], [35.225105, -119.25568]];
L.imageOverlay(imageUrl, imageBounds, {
    opacity: 0.8
}).addTo(mymap);

//sound support
var chatSounds = document.getElementById("chatSounds");


function retrieveSession(){
    let session = {};
    session.name = sessionStorage.getItem("chat-session-name");
    session.color = sessionStorage.getItem("chat-session-color");
    session.room = sessionStorage.getItem("chat-session-room");
    return session;
}

function scrollToBottom () {
    // Selectors
    var messages = jQuery('#messages');
    var newMessage = messages.children('li:last-child');
    // Heights
    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight();

    if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
        messages.scrollTop(scrollHeight);
    }
}




socket.on('connect', function () {
    console.log('Connected to server');
    connected = true;
    var params = retrieveSession();//$.deparam(window.location.search);
    socket.emit('join',params,function(err){
        if(err){
            alert(err)
            window.location.href = "/";
        }else{
            console.log('No Join error')
        }
    })

    if (!navigator.geolocation) {
        return alert('Geolocation not supported by your browser.')
    }
    navigator.geolocation.getCurrentPosition(function (position) {

        mymap.panTo([position.coords.latitude, position.coords.longitude]);

    }, function (err) {
        alert('Please enable GPS.')
    });

});

socket.on('disconnect', function () {
    console.log('Disconnected from server');
    connected = false;
});


socket.on('updateUserList', function(users){
    console.log('Users list', users)
    var ol = $('<ol></ol>');

    users.forEach(function(user){
        ol.append($('<li></li>').text(user))
    });

    $('#users').html(ol);
});

// socket.on('newEmail', function (response) {
//     console.log('email arrived:', response)
// });

socket.on('newMessage', function (message) {
    chatSounds.play();
    var formattedTime = moment(message.createdAt).format('h:mm a');
    var template = $('#message-template').html();
    var html = Mustache.render(template,{
        from: message.from,
        text: message.text,
        createdAt: formattedTime
    });
    $('#messages').append(html);
    scrollToBottom();
});


// socket.emit('createMessage',{
//     from:'frank',
//     text:'hi'
// },function (data){
//     console.log('got it', data);
// });




///////////////////////


// socket.on('newLocationMessage',function(message){
//     var formattedTime = moment(message.createdAt).format('h:mm a');
//     var template = $('#location-message-template').html();
//     var html = Mustache.render(template,{
//         from: message.from,
//         url: message.url,
//         createdAt: formattedTime
//     });
//     $('#messages').append(html);
//     scrollToBottom();
// });


$('#message-form').on('submit', function (e){
    e.preventDefault();
    var messageTextbox = $('[name=message]');
    if(connected) {
        socket.emit('createMessage', {
            from: 'client',
            text: messageTextbox.val()
        }, function () {
            messageTextbox.val('')//set to blank
        })
    }else{
        alert('Unable to communicate with server.')
    }
});

var locationButton = $('#send-location');
locationButton.on('click',function(){
    // if(!navigator.geolocation){
    //     return alert('Geolocation not supported by your browser.')
    // }
    locationButton.attr('disabled','disabled').text('Sending location...');

    navigator.geolocation.getCurrentPosition(function(position){
        locationButton.removeAttr('disabled').text('Send Location');
        socket.emit('createLocationMessage',{
            lat: position.coords.latitude,
            lng: position.coords.longitude
        });
    },function(err){
        locationButton.removeAttr('disabled').text('Send Location');
        alert('Unable to fetch location.')
    })
});



// LIVE MAP STUFF //////









function markerIcon(from, color){
    //format: 'markers/blue_MarkerA.png'
    return L.icon({iconUrl: `markers/${color}_Marker${from.charAt(0)}.png`,});
}

function formattedBubble(from, updatedAt){
    let formattedTime = moment(updatedAt).format('h:mm:ss a');
    return `<b>${from}</b><br><small>${formattedTime}</small>`;
}
function formattedLandmarkBubble(from, label, updatedAt){
    let formattedTime = moment(updatedAt).format('h:mm:ss a');
    if(label === ''){
        return `<b> - ${from}</b><br><small>${formattedTime}</small>`;
    }
    return `${label}<br><b> - ${from}</b><br><small>${formattedTime}</small>`;

}

function updateMarker(userData){
    if( markers.has(userData.from) ){
        markers.get(userData.from).setLatLng(userData.latLng).addTo(mymap)
            .bindPopup(formattedBubble(userData.from,userData.updatedAt));
    }else{
        markers.set(
            userData.from,
            L.marker(userData.latLng, {icon: markerIcon(userData.from, userData.color)}).addTo(mymap)
                .bindPopup(formattedBubble(userData.from,userData.updatedAt))
        );
    }
}


function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
}

function sendUpdateLocation(){
    let lat = 37.645 + parseFloat((Math.random() / 1000).toFixed(5));
    let lng = -122.424 + parseFloat((Math.random() / 1000).toFixed(5));
    socket.emit('testLocationUpdate',[lat,lng]);
}
function updateLocation(){

    let lat = 37.645 + parseFloat((Math.random() / 1000).toFixed(5));
    let lng = -122.424 + parseFloat((Math.random() / 1000).toFixed(5));

    mymap.panTo([lat,lng]);//.setView([lat,lng], 17, { animation: true });

    marker.setLatLng([lat, lng]).addTo(mymap)
        .bindPopup(text);//.openPopup();
}

socket.on('updateLocation',function(userData){
    // mymap.panTo(userData.latLng);

    updateMarker(userData);
    // marker.setLatLng(userData.latLng).addTo(mymap)
    //     .bindPopup(formattedBubble(userData.from,userData.updatedAt));
});

// let landmarkIcon = L.icon({iconUrl: 'landmarkers/marina.png',});

// let text = "<b>Adam</b><br><small>I'm here now!</small>";
socket.on('updateLandMarker',function(userData){

    let marker_img ='landmarkers/'+userData.landmark+'.png';

    if( markers.has(userData.landmark) ){
        markers.get(userData.landmark).setLatLng(userData.latLng).addTo(mymap)
            .bindPopup(formattedLandmarkBubble(userData.from,userData.label,userData.updatedAt));
    }else{
        markers.set(
            userData.landmark,
            L.marker(userData.latLng, {icon: L.icon({iconUrl: marker_img,})}).addTo(mymap)
                .bindPopup(formattedLandmarkBubble(userData.from,userData.label,userData.updatedAt))
        );
    }
});

socket.on('deleteLandmark',function(marker_key){
    if(markers.has(marker_key)) {
        mymap.removeLayer(markers.get(marker_key));
        markers.delete(marker_key);
    }
});

socket.on('deleteUser',function(user_key){
    if(markers.has(user_key)) {
        mymap.removeLayer(markers.get(user_key));
        markers.delete(user_key);
    }
});


var landmarker = 'marina';
var landmark_label = '';



function send_landmarker(e){
    data = {landmark: landmarker, label: landmark_label, location: e.latlng};
    landmarker = 'marina';
    landmark_label= '';
    document.getElementById('landmarker').src = "/landmarkers/blank.png";
    document.getElementById('landmark_select').value = "blank";
    document.getElementById('landmark_label').value = "";
    $('#landmark_select').removeAttr('disabled');
    $('#landmark_label').removeAttr('disabled');
    $('#set-marker').removeAttr('disabled').text('Select');

    socket.emit('sendLandmarker',data, (response)=>{
        alert(response);
    });
}

// mymap.on('click', drop_anchor);
mymap.on('click', send_landmarker);

function refocusMap(){
    mymap.invalidateSize(true); //todo mentioned solution
}

// GEO LOCATION LIVE PING //
setInterval(function(){
    if(connected) {
        // if (!navigator.geolocation) {
        //     return alert('Geolocation not supported by your browser.')
        // }


        navigator.geolocation.getCurrentPosition(function (position) {

            socket.emit('locationUpdate', [position.coords.latitude, position.coords.longitude]);


        }, function (err) {

        })
    }
}, 10000);


setInterval(function(){

    if (!navigator.geolocation) {
        return alert('Geolocation not supported by your browser.')
    }
    // locationButton.attr('disabled','disabled').text('Sending location...');

    navigator.geolocation.getCurrentPosition(function (position) {
        mymap.panTo([position.coords.latitude, position.coords.longitude]);



    }, function (err) {

    })

}, 60000);


var setButton = $('#set-marker');
var landmarkSelect = $('#landmark_select');
var landmarkLabel = $('#landmark_label');
var resetButton = $('#reset-marker');
var deleteButton = $('#delete-marker');
var resendLandmarksButton = $('#resend-landmarks');

setButton.on('click',function() {
    if(landmarker === 'marina'){
        alert("Select Landmark");
        return;
    }
    landmark_label = document.getElementById('landmark_label').value;
    landmarkSelect.attr('disabled', 'disabled');
    landmarkLabel.attr('disabled', 'disabled');
    setButton.attr('disabled', 'disabled').text('tap map to set landmark');
});

resetButton.on('click',function() {
    landmarker = 'marina';
    landmark_label = '';
    document.getElementById('landmarker').src = "/landmarkers/blank.png";
    document.getElementById('landmark_select').value = "blank";
    document.getElementById('landmark_label').value = "";
    landmarkSelect.removeAttr('disabled');
    landmarkLabel.removeAttr('disabled');
    setButton.removeAttr('disabled').text('Select');
});

deleteButton.on('click',function() {
    document.getElementById('landmarker').src = "/landmarkers/blank.png";
    document.getElementById('landmark_select').value = "blank";
    socket.emit('deleteMarker',landmarker, (response)=>{
        if(response){
            if(landmarker === 'marina'){
                alert('Anchor removed from group.')
            }else{
                alert('Landmark removed from group.')
            }
        }
        landmarker = 'marina';
    });

    //todo reset timer
});

resendLandmarksButton.on('click',function(){
    resendLandmarksButton.attr('disabled', 'disabled').text('resending to group...');
    socket.emit('resendLandmarks',undefined, ()=>{
        resendLandmarksButton.removeAttr('disabled').text('Resend Landmarks');
        alert('Landmarks resent to group.')
    });
    //todo reset timer
});

function setImage(value) {
    document.getElementById('landmarker').src="/landmarkers/"+value.value+".png";
    landmarker = value.value;
}