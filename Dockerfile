FROM node:13.3-alpine

RUN apk add --no-cache bash
RUN apk add --no-cache tini

# Set ENV variables

ENV PORT 3000
EXPOSE 3000

WORKDIR /app

COPY package*.json ./

RUN npm install && npm cache clean --force

COPY . .


ENTRYPOINT ["tini", "--"]
CMD [ "node", "./server/server.js" ]
