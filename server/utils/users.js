class Users {
    constructor () {
        this.users = [];
    }


    addUser (id, name, room) {
        let user = {id,name,room};
        this.users.push(user);
        return user;
    }
    addUserWithColor (id, name, room,color) {
        let user = {id,name,room,color};
        this.users.push(user);
        return user;
    }

    removeUser(id){
        let user = this.getUser(id);
        if(user){
            this.users = this.users.filter((user)=>user.id !== id);
        }
        return user;
    }


    getUser(id){
        return this.users.filter((user)=> user.id === id)[0]
    }

    getUserId(name, room){
        return this.users.filter((user)=> user.name === name && user.room === room)[0]
    }


    getUserList(room){
        let user_list = this.users.filter( (user) => user.room === room);
        let names_array = user_list.map((user)=>user.name);
        return names_array;
    }
}

module.exports = {Users};


