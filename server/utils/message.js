const moment = require('moment');

const generateMessage = (from,text)=>{
    return {
        from,
        text,
        createdAt: moment().valueOf()
    }
};


const generateLocationMessage = (from, lat, lng) =>{
    return {
        from,
        url:`https://www.google.com/maps?q=${lat},${lng}`,
        createdAt: moment().valueOf()
    }
};

const generateLiveLocation = (from, color, latLng) =>{
    return {
        from,
        color: color,
        latLng: latLng,
        updatedAt: moment().valueOf()
    }
};

const generateLandmarkLocation = (from, landmark, label, latLng) =>{
    return {
        from,
        landmark: landmark,
        label: label,
        latLng: latLng,
        updatedAt: moment().valueOf()
    }
};

module.exports = {generateMessage,generateLocationMessage,generateLiveLocation,generateLandmarkLocation};