const expect = require('expect');
const {generateMessage,generateLocationMessage} = require('./message')
describe('generateMessage',()=>{
    it('should generate correct message object',()=>{
        let from = 'john';
        let text = 'I am alive';
        let message = generateMessage(from,text);

        expect(typeof message.createdAt).toBe('number');
        expect(message).toMatchObject({from,text});
    })
});


describe('generateLocationMessage',()=>{
    it('should generate correct location object',()=>{
        let from = 'Dev';
        let lat = 15;
        let lng = 19;
        let url = `https://www.google.com/maps?q=${lat},${lng}`;
        let locationMessage = generateLocationMessage(from,lat,lng);

        expect(typeof locationMessage.createdAt).toBe('number');
        expect(locationMessage).toMatchObject({from,url});
    })
});