const isRealString = (str) =>{
    return typeof str === 'string' && str.trim().length > 0;
};

const capitalize = (str) =>{
    if (typeof str !== 'string') return '';
    return str.charAt(0).toUpperCase() + str.slice(1);
};

const isValidWord = (word) =>{
    if(word.length > 16) return false;
    let regex = /[^a-zA-Z]/g;
    let result = word.match(regex);
    return !result;
};

const isValidLabel = (label) =>{
    if(label.length > 20) return false;
    let regex = /[^a-zA-Z !?.']/g;
    let result = label.match(regex);
    return !result;
};
const isValidColor = (color) =>{
    let colors = ["blue", "brown", "darkgreen", "green", "orange", "paleblue", "pink", "purple", "red", "yellow"];
    return colors.includes(color);
};

const isValidLandmark = (landmark) =>{
    let landmarks = ["marina", "flag", "camp", "landmark", "tree", "snowflake", "fire", "water", "waterfalls", "food", "bar", "toilets", "parking", "question", "caution"];
    return landmarks.includes(landmark);
};
module.exports = {isRealString, capitalize,isValidWord,isValidColor,isValidLandmark, isValidLabel};