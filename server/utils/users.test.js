const expect = require('expect');

const {Users} = require('./users');

describe('Users',()=>{
    var test_users;

    beforeEach(()=>{
        test_users = new Users();
        test_users.users = [{
            id:'1',
            name:'Adam',
            room:'Apple'
        },{
            id:'2',
            name:'Ben',
            room:'Banana'
        },{
            id:'3',
            name:'Carly',
            room:'Apple'
        }];
    });

// ADD USER (id,name,room)
    it('should add new user',()=>{
        var users = new Users();

        let test_user = {
            id:'123',
            name:'Adam',
            room:'Room21'
        };

        res_user = users.addUser(test_user.id,test_user.name,test_user.room);

        expect(users.users).toMatchObject([test_user]);
        expect(res_user).toMatchObject(test_user);
    });

// GET USER LIST (room)
    it('should return names for node course',()=>{
        var test_user_list = test_users.getUserList('Apple')

        expect(test_user_list).toMatchObject(['Adam','Carly'])
    });

// REMOVE USER (id)
    it('should remove user',()=>{
        let test_id = '1';
        let removed_length = test_users.users.length -1;
        var removed_user = test_users.removeUser(test_id);

        expect(removed_user.id).toBe(test_id);
        expect(test_users.users.length).toBe(removed_length)
    });


    it('should NOT remove user',()=>{
        let test_id = '99';
        let unchanged_length = test_users.users.length;
        var removed_user = test_users.removeUser(test_id);

        expect(removed_user).toBeFalsy();
        expect(test_users.users.length).toBe(unchanged_length)
    });

// GET USER (id)
    it('should find user',()=>{
        var test_id = '2';
        var test_user = test_users.getUser(test_id);

        expect(test_user.id).toBe(test_id)
    });


    it('should NOT find user',()=>{
        var test_id = '-1';
        var test_user = test_users.getUser(test_id);

        expect(test_user).toBeFalsy()
    });
});