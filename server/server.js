const express = require('express');
const http = require('http');
const path = require('path');
const socketIO = require('socket.io');

const {generateMessage, generateLocationMessage, generateLiveLocation, generateLandmarkLocation} = require('./utils/message');
const {isRealString, isValidColor, isValidWord, isValidLabel, isValidLandmark, capitalize} = require('./utils/validation');
const {Users} = require('./utils/users');

const __public = path.join(__dirname,'../public');
const PORT = process.env.PORT || 80;


const app = express();

const server = http.createServer(app);
const io = socketIO(server);
var users = new Users();
var landmarks = new Map();


app.use(express.static(__public));

app.get('/main', function(req, res) {
    res.sendFile(__public+'/main.html');
});

//io.emit = to everyone , ->io.to(room).emit
//socket.broadcast.emit = sends to everyone connected except current user, ->socket.broadcast.to(room).emit
//socket.emit = specifically to current user

io.on('connection', (socket) => {



    socket.on('join', (params,callback) => {
        if(!isRealString(params.name) || !isRealString(params.room) || !isValidWord(params.name) || !isValidWord(params.room) || !isRealString(params.color)|| !isValidColor(params.color)){
            return callback('You need to authenticate.');
        }
        params.name = capitalize(params.name);
        params.room = capitalize(params.room);
        if(users.getUserId(params.name,params.room)){
            return callback(`${params.name} is already in ${params.room} Chat.`);
        }
        socket.join(params.room);
        users.removeUser(socket.id);
        users.addUserWithColor(socket.id, params.name, params.room, params.color);

        io.to(params.room).emit('deleteUser',users.getUser(socket.id).name); //remove old marker
        io.to(params.room).emit('updateUserList', users.getUserList(params.room));
        // GREETING
        socket.emit('newMessage',generateMessage( `${params.room} Chat`,`You have joined ${params.room} Chat!`));

        socket.broadcast.to(params.room).emit('newMessage',generateMessage(`${params.room} Chat`,`${users.getUser(socket.id).name} has joined.`));

        console.log(`${users.getUser(socket.id).name} connected`);
        callback();
    });

    socket.on('createMessage', (message, callback) => {
        let user = users.getUser(socket.id);
        if(user && isRealString(message.text)){

            if(message.text.charAt(0) === '/'){ //manual remove disconnected user marker
                cmd = message.text.split(' ');
                if( cmd.length === 2 && cmd[0] === '/remove'){
                    io.to(user.room).emit('deleteUser',capitalize(cmd[1]));
                }
            }else if(message.text.charAt(0) === '@') { //private message feature
                userData = message.text.split(' ');
                if(userData.length >= 2){
                    let recipient = users.getUserId(capitalize(userData[0].substr(1,(userData[0].length-1))), user.room);
                    if(recipient){
                        io.to(recipient.id).emit('newMessage',generateMessage(user.name,message.text));
                        io.to(user.id).emit('newMessage',generateMessage(user.name,message.text));
                    }
                }
            }else{
                    io.to(user.room).emit('newMessage',generateMessage(user.name,message.text));
            }
        }
        callback('server response');

    });

    socket.on('createLocationMessage', (location)=>{
        let user = users.getUser(socket.id);
        if(user){
            io.to(user.room).emit('newLocationMessage',generateLocationMessage(user.name, location.lat, location.lng))
        }
    });


    socket.on('locationUpdate', (location)=>{
        let user = users.getUser(socket.id);
        if(user){
            io.to(user.room).emit('updateLocation',generateLiveLocation(user.name, user.color, location))
        }
    });

    // socket.on('dropAnchor', (location)=>{
    //     console.log(location);
    //     let user = users.getUser(socket.id);
    //     if(user){
    //         landmarks.set('marina',generateLiveLocation(user.name, user.color, location));
    //         io.to(user.room).emit('updateLandMarker',generateLiveLocation(user.name, user.color, location))
    //     }
    // });

    socket.on('sendLandmarker', (data, callback)=>{
        if(!isValidLandmark(data.landmark) || !isValidLabel(data.label)){
            return callback('Invalid request.');
        }
        let user = users.getUser(socket.id);
        if(user){

            landmarks.set(data.landmark, generateLandmarkLocation(user.name, data.landmark, data.label, data.location));
            io.to(user.room).emit('updateLandMarker',generateLandmarkLocation(user.name, data.landmark, data.label, data.location))
        }
    });


    socket.on('resendLandmarks', (undefined, callback)=>{
        let user = users.getUser(socket.id);
        if(user){
            landmarks.forEach((data)=>{
                io.to(user.room).emit('updateLandMarker',data)
            },user.room);
            callback();//notify client resend complete
        }

    });

    socket.on('deleteMarker', (landmark, callback)=>{
        let user = users.getUser(socket.id);
        if(user){
            if( landmarks.has(landmark) ){
                landmarks.delete(landmark);
                io.to(user.room).emit('deleteLandmark',landmark);
                callback(true);
            }
            callback();
        }

    });

    // DISCONNECT
    socket.on('disconnect', () => {
        let removed_user = users.removeUser(socket.id);

        if(removed_user){
            io.to(removed_user.room).emit('updateUserList', users.getUserList(removed_user.room));
            let message = `${removed_user.name} has left.`
            io.to(removed_user.room).emit('newMessage', generateMessage(`${removed_user.room} Chat`,message));

            console.log(`${removed_user.name} disconnected`);
        }


    });
});

server.listen(PORT,()=>{
    console.log('app active')
});
